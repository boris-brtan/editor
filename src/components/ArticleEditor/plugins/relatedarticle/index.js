import ArticleEditor from '../../article-editor'
import './relatedarticle.css'

ArticleEditor.add('block', 'block.relatedarticle', {
    mixins: ['block'],
    type: 'paragraph',
    emptiable: true,
    toolbar: {
        add: { command: 'addbar.popup', title: '## buttons.add ##' },
        edit: { command: 'relatedarticle.edit', title: '## buttons.edit ##' }
    },
    control: {
        trash: { command: 'block.remove', title: '## buttons.delete ##' },
        duplicate: { command: 'block.duplicate', title: '## buttons.duplicate ##'  },
    },
    getId: function() {
        return this.getBlock().attr('data-id')
    },
    create: function({id}) {
        return this.dom(`<div>
            <span>${this.lang.get('relatedarticle.title')}: </span>
            <span>${id}</span>
        </div>`)
            .addClass('relatedarticle card')
            .attr('arx-type', 'relatedarticle', true)
    }
})
ArticleEditor.add('plugin', 'relatedarticle', {
    stop: function() {},
    translations: {
        en: {
            relatedarticle: {
                title: "Related article",
                save: "Save",
                cancel: "Cancel",
                insert: "Insert"
            }
        }
    },
    defaults: {
        endpoint: '/assets/articles.json',
    },
    popups: {
        add: {
            title: '## relatedarticle.title ##',
            width: '100%',
            footer: {
                cancel: { title: '## relatedarticle.cancel ##', command: 'popup.close' }
            }
        },
        edit: {
            title: '## relatedarticle.title ##',
            width: '100%',
            footer: {
                save: { title: '## relatedarticle.save ##', command: 'relatedarticle.save', type: 'primary' },
                cancel: { title: '## relatedarticle.cancel ##', command: 'popup.close' }
            },
        }
    },
    start: function() {
        this.app.addbar.add('relatedarticle', {
            title: '## relatedarticle.title ##',
            icon: '<svg width="16" focusable="false" data-prefix="fas" data-icon="file-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path fill="currentColor" d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z" class=""></path></svg>',
            command: 'relatedarticle.popup'
        })
    },
    popup: async function() {
        let list
        try {
            list = await (await fetch(this.opts.relatedarticle.endpoint)).json()
        } catch (err) {
            console.error(err)
            list = []
        }
        let stack = this.app.popup.add('relatedarticle', this.popups.add)
        this.$box = this.dom('<table><thead><tr><th></th><th>id</th><th>title</th></tr></thead></table>')
        stack.getBody().append(this.$box)
        list.forEach(({id, title}) => {
            const tr = this.$box.nodes[0].insertRow()
            const button = tr.insertCell()
            tr.insertCell().innerText = id
            tr.insertCell().innerText = title
            button.innerHTML = `<button class="arx-form-button">Select</button>`
            button.onclick = () => {
                this.insert(id)
            }
        })
        stack.open()
    },
    edit: async function(params, button) {
        let list
        try {
            list = await (await fetch(this.opts.relatedarticle.endpoint)).json()
        } catch (err) {
            console.error(err)
            list = []
        }
        let stack = this.app.popup.create('relatedarticle', this.popups.edit)
        this.$box = this.dom('<table><thead><tr><th></th><th>id</th><th>title</th></tr></thead></table>')
        stack.getBody().append(this.$box)
        list.forEach(({id, title}) => {
            const tr = this.$box.nodes[0].insertRow()
            const button = tr.insertCell()
            tr.insertCell().innerText = id
            tr.insertCell().innerText = title
            button.innerHTML = `<button class="arx-form-button">Select</button>`
            button.onclick = () => {
                this.save(id)
            }
        })

        // open
        this.app.popup.open({ button })
    },
    insertFromSelect: function(e) {
        e.preventDefault()

        let $target = this.dom(e.target)
        this._buildBoxItem($target.attr('data-url'))
    },
    insert: function(id) {
        this.app.popup.close()

        let instance = this.app.create('block.relatedarticle', null, {id})
        instance.getBlock().attr('id', id, true)
        this.app.block.add({ instance })
    },
    save: function(id) {
        let current = this.app.block.get()
        let value = current.getBlock().find('span:last-child')
        value.html(id)
        current.getBlock().attr('id', id, true)
        this.app.popup.close()
    }
})